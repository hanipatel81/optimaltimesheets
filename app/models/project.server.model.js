'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var TrimmedString = {type:String, trim:true};

/**
 * Project Schema
 */
var ProjectSchema = new Schema({
	vendor: {
		type: Schema.ObjectId,
		ref: 'Vendor',
		required: 'Please select a vendor'
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User',
		required: 'Please select a user'
	},
	startDate: TrimmedString,
	endDate: TrimmedString,
    client: { type: String, required: 'Please fill Client Name',trim: true},
    location: {
        effectiveDate: Date,
		line1: TrimmedString,
        line2:TrimmedString,
        city: TrimmedString,
        zip: TrimmedString,
        state: TrimmedString,
        country: TrimmedString
    },
    docs: [{
        type: TrimmedString,
        file: TrimmedString,
		s3error: Boolean,
        _id:false
    }],
	rate:[{effectiveDate: Date, rate: Number }],
    timesheetCycle:TrimmedString,
    weekStartDay:TrimmedString, /* If weekly need to identify Sun/Mon etc or date for bi-weekly*/

	netDue: Number,
	invoiceCycle:TrimmedString,
	invoiceWeekStartDay:TrimmedString, /* If weekly need to identify Sun/Mon etc or date for bi-weekly*/
	invoicePrefix:TrimmedString,
	lastInvoice:{
		date:Date,
		amount:Number,
		name:TrimmedString
	},
	created: {
		type: Date,
		default: Date.now
	}
});

ProjectSchema.statics.getList = function(callback){
    this.find({}).populate('vendor').populate('user','displayName firstName lastName').lean(true).exec(function(err,projects){
        if(err){
            return callback(err);
        } else {
            callback(undefined, projects);
        }
    });
};

mongoose.model('Project', ProjectSchema);