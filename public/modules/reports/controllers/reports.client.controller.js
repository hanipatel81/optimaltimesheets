'use strict';
/*globals _,moment */
// Report controller
angular.module('reports').controller('ReportsController', ['$scope', '$timeout', '$window', '$stateParams', '$location', '$http', 'Authentication', 'Users', 'Projects',
	function($scope, $timeout, $window, $stateParams, $location, $http, Authentication, Users, Projects) {
		$scope.authentication = Authentication;
		$scope.projects = Projects.getList();
		$scope.report = {};
		
		/* **********************************
		* converts object in to proper format.
		* group by user, then project, then status
		* after that merge daily into one - sort by date.
		* ************************************/
		var generateTsReport = function(data,start,end){
			var statuses = [];
			start = start?moment(start).format('YYYY-MM-DD'):'0';
			end = end?moment(end).format('YYYY-MM-DD'):'9';
			var compareDate = function (a, b) {
				if (a.date > b.date) { return 1;}
				if (a.date < b.date) {return -1;}
				return 0;
			};
			data = _.groupBy(data,function(obj){return obj.project;}); // group by project/placement
			_.forEach(data, function(obj , key){ // for each project/placement
				data[key] = _.groupBy(obj,function(obj){return obj.status;}); //Group by Status
				var tsData = data[key]; // Status wise grouped data
				var total = 0;
				_.forEach(tsData,function(obj,key){ // obj is array of timesheets
					statuses.push(key);
					tsData[key] = {};
					
					// Adding daily hours
					tsData[key].daily = _.flatten(_.map(obj,'daily')).sort(compareDate);
					
					// Rmeove out of range dates on from the daily list.
					_.remove(tsData[key].daily, function(day) {
						if(day.date < start || day.date > end){ return true;}
					});
					
					// Calculating Total hours
					tsData[key].hours = _.reduce(tsData[key].daily, function(result, day) {
						return result+ (day.hours?day.hours:0);
					}, 0);
					
					// Adding documents 
					tsData[key].docs = _.flatten(_.map(obj,'docs'));
					
					total += tsData[key].hours;
				});
				data[key].hours = total;
				
				// Need to remove $scope dependancy
				data[key].project = _.find($scope.projects, { _id: key });
			});
			
			// Need to remove $scope dependancy
			$scope.tsReport = data;
			$scope.statuses = _.uniq(statuses);
		};
		
		$scope.downloadAll = function(row,status){
			var files = [];
			if(status){
				files = row[status].docs;
			} else {
				files = [];
				_.forEach(row, function(obj , key){
					if(obj && obj.docs){
						files = files.concat(obj.docs);		
					}
				});
			}
			if(files.length){
				$scope.formData = {
					files: files,
					name:row.project.user.displayName,
					startDate: $scope.report.startDate,
					endDate: $scope.report.endDate
				};
				$timeout(function () {
				    $window.document.forms.downloadForm.submit();
				});
				/*
				$http.post('/fileUpload/downloadZip', {
					files: files,
					name:row.project.user.displayName,
					startDate: $scope.report.startDate,
					endDate: $scope.report.endDate
				});*/
			}
		};
		
		$scope.timeSheetReport = function(){
			delete $scope.timesheets;
			$http.post('/reports/timesheets', $scope.report).
				success(function(data, status, headers, config) {
					generateTsReport(data,$scope.report.startDate,$scope.report.endDate);
				}).
				error(function(data, status, headers, config) {
					alert('Error while getting report');
				});
		};
	}
]);
