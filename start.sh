#!/bin/sh

if [ $(ps -e -o uid,cmd | grep $UID | grep node | grep -v grep | wc -l | tr -s "\n") -eq 0 ]
then
		export PATH=/usr/local/bin:$PATH
		export NODE_ENV="development"
		export awsAccessKeyId=""
		export awsSecretAccessKey=""
		export awsRegion="us-east-1"
		export awsBucket=""
		export MONGOLAB_URI=""
		export PORT=""
		#current_dir=$(pwd)
		script_dir=$(dirname $0)
		cd $script_dir
		forever start server.js
fi